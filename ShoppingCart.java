/*
 * This program ivolves building a shopping cart without the use of bags. 
 * Items can be added to the cart, the cart can be viewed, items can be removed from the cart, and you can procede to "checkout" where detailed price information is displayed.
 */
package data.structures;

import java.util.Scanner;

public class ShoppingCart{
    public static void main(String[] args){
        int[] itemID = new int[] {0,1,2,3,4,5,6,7,8,9}; //Stores item id numbers, will be used to add items to the cart. 
        String[] itemList = new String[]{"Textbook", "Pencil", "Pen", "Notebook", "Highlighter", "Binder", "Folder", "Gum", "Paperclips", "Ruler"}; //item names
        int[] itemPrice = new int[] {50, 1, 2, 8, 2, 5, 3, 2, 4, 2}; // item prices
        for(int a=0;a<10;a++){ //for loop will display all of the information contained in the above arrays. 
        System.out.println("Item ID:" +itemID[a] +" - " +itemList[a] +" - $" +itemPrice[a]);
                
        }
        
        String[] cart; //new array called cart that will store 
        cart = new String[10];
        int cartCapacity=10; //maximum cart capacity. You cannot add more than 10 items to the cart. 
        int cartItems=0; //initially there are no items in the cart
        int exitLoop=0; //this will be used to go to checkout after viewing the cart. (when exitLoop =1)
        int itemToAdd; 
        int itemToRemove; 
        double subtotal=0; //subtotal starts at 0, price will be added as items are added
        
        while ((cartItems<=cartCapacity) && exitLoop!=1){
            Scanner scanner=new Scanner(System.in);
            System.out.println("Enter 0 to View Cart, 1 to add an item to your cart, or 2 to remove an item from cart: ");
            int action = scanner.nextInt(); //user inputs what they want to do

            if(action==0){ //shows contents of the cart
                if(cartItems==0){
                    System.out.println("Your cart is empty.");
                    cartItems=0;
                }
                else{
                    System.out.println("Your cart contains "+cartItems +"/"+cartCapacity +" items:");
                    for(int b=1; b<cartItems+1;b++){
                    System.out.println(cart[b]);
                    }
                    System.out.println("Press 1 to check out or 0 to continue shopping"); // option to check out or continue shopping
                    exitLoop=scanner.nextInt();
                }
            }
            else if(action==1){ //Add an item to the cart dialog
                System.out.println("Enter the item ID # to add it to your cart");
                itemToAdd=scanner.nextInt(); //takes in input from user
                cart[cartItems+1]=itemList[itemToAdd]; //adds the item to the cart array
                cartItems++; //Increases the number of items in the cart
                subtotal+=itemPrice[itemToAdd]; //increases the subtotal but whatever price the item added was
            }
            else if(action==2){
                System.out.println("Your cart contains:"); // shows contents of the cart, and shows a number id that can be used to remove each item from the cart. 
                for(int c=1;c<(cartItems+1);c++){
                    System.out.print(c+" ");
                    System.out.println(cart[c]);
                }
                System.out.println("Enter the item # to remove it from your cart"); //gets input from user after dialog
                itemToRemove=scanner.nextInt();
                cart[itemToRemove]=" ";
                cart[cartItems+1]=cart[itemToRemove];
                
                for(int h=itemToRemove;h<cartItems;h++){ //reorders the array! 
                    String temp=cart[h+1];
                    cart[h+1] = cart[h];
                    cart[h]=temp;
                }
                
                cart[cartItems+1]="";
                
                cartItems--; //subtracts one from the number of items in the cart
                subtotal-=itemPrice[itemToRemove]; //subtracts the item price from the subtotal 
            }
        }
    

           System.out.println("Your cart contains:");
            for(int d=1;d<(cartItems+1);d++){
            System.out.print(d+" ");
            System.out.println(cart[d]);
            }
            
            System.out.println("Subtotal: $" +subtotal);
            double tax=(subtotal*0.06);
            System.out.println("Tax: $"+tax);
            System.out.println("Total: $"+(tax+subtotal));
    }
}
